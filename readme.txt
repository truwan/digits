﻿Требования к проекту "Распознавание рукописных цифр"
Трубецков Иван 4103

Программа должна распознавать цифру по изображению. Программа должна быть реализована на основе нейронных сетей и должна уметь обучаться по начальному набору данных.
При запуске программы перед пользователем должно появляться окошко с меню и кнопкой. С помощью меню пользователь открывает нужное ему изображение. Выбранное изображение отображается в окошке программы. При нажатии на кнопку программа выдает изображенную цифру во всплывающем окне. В этом окне также должны располагаться 2 кнопки: «Выбрать еще одно изображение» и «Завершить работу».  С помощью кнопки «Выбрать еще одно изображение» пользователь открывает еще одно нужное ему изображение. При нажатии на кнопку «Завершить работу» программа должна прекратить работу.



