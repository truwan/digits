import NN
import scipy.io as sio
import mnist_loader

training_data, validation_data, test_data = mnist_loader.load_data_wrapper()

##mat_contents = sio.loadmat('ex3data1.mat')
###print(mat_contents)
##X = mat_contents['X']
##print('X = ',X)
###print('X_shape = ',X.shape) # размер
##y = mat_contents['y']
##print('y = ',y)
##
##    #  ex3weights.mat
##mat_contents = sio.loadmat('ex3weights.mat')
###print(mat_contents)
##Theta1 = mat_contents['Theta1']
##Theta2 = mat_contents['Theta2']
##print('Theta1: ',Theta1)
##print('Theta2: ',Theta2)

##training_data = zip(X,y)
##print("training_data= ", training_data)

net = NN.Network([784,30,10])
net.SGD(training_data,30,10,3.5,test_data=test_data)
net.evaluate(validation_data)
