from PyQt4 import QtGui,QtCore
from PyQt4.QtGui import QMessageBox, QLabel, QImage, QPixmap
from PyQt4.QtCore import QPointF

import sys, math, copy

import matplotlib.pyplot as plt
import matplotlib.image as mpimg

import NN
import mnist_loader
import numpy as np

width = 400
length = 400


class MainWindow(QtGui.QMainWindow):
    ## Конструктор
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        
        self.setGeometry(100,100,length,width)
        self.setWindowTitle('DIGITS')
##        self.NetFlag= 0
        self.PicFlag = 0
        self.biases = np.load('biases.txt.npy')
        self.weights = np.load('weights.txt.npy')
    #Menubar actions
        OpenFile = QtGui.QAction('Open File', self)
        OpenFile.setShortcut("Ctrl+O")
        OpenFile.triggered.connect(self.OpenFileAction)
        
##        Learn = QtGui.QAction('Learn', self)
##        Learn.setShortcut("Ctrl+L")
##        Learn.triggered.connect(self.LearnAction)
        

    #Menubar
        MenuBar = self.menuBar()
        FileMenu = MenuBar.addMenu('File')
        FileMenu.addAction(OpenFile)
        #FileMenu.addAction(Learn)

    
    #Run Button
        self.RunButton = QtGui.QPushButton('Run',self)
        self.RunButton.resize(self.RunButton.sizeHint())
        self.RunButton.move(length/2 - 25,width-50)
        self.RunButton.clicked.connect(self.RunAction)
        
    ## Запуск обучения нейронной сети
##    def LearnAction(self):
##        print("in learn action")
##        training_data, validation_data, test_data = mnist_loader.load_data_wrapper()
##        self.net = NN.Network([784,30,10])
##        #self.net.SGD(training_data,30,10,3.0,test_data=test_data)
##        self.net.SGD(training_data,5,10,3.0)
##        self.NetFlag = 1

    ## Обработка нажатя на кнопку "Run"
    def RunAction(self):
        print('In Run Action')
        if (self.PicFlag == 0):
            print("you have to choose picture")
##        elif (self.NetFlag == 0):
##            print("you have to learn your network")
        else:
            self.net = NN.Network([784,30,10],self.biases,self.weights)
            digit = np.argmax(self.net.feedforward(self.picture))
            self.ShowResult(digit)

    ## Вывод реультата на экран
    # @param digit значение цифры,изображенной на каритнке
    def ShowResult(self,digit):
       # print('here')
        msg = QMessageBox()
        msg.setWindowTitle("Your Digit is")
        msg.setText(str(digit))
        msg.addButton('Open another file',QMessageBox.YesRole)
        msg.addButton('Quit',QMessageBox.NoRole)
        ret = msg.exec_()
        #print(ret)
        
    #Quit Button
        if ret == 1:
            #print('hello')
            sys.exit()
            
    # Open another file Button
        if ret == 0:
            #print('hey')
            self.OpenFileAction()
            
        
        
    ## Открытие файла
    def OpenFileAction(self):
        print('In Open File')
        name = QtGui.QFileDialog.getOpenFileName(self, 'Open File')
        img = QImage(name)
        print('image size', img.width(), img.height())
        self.label = QLabel(self)
        self.label.setPixmap(QPixmap(img))
        self.label.setGeometry(20, 20, 325, 325)
        self.label.setScaledContents(True)
        self.label.show()
        img = mpimg.imread(name)
        self.picture = np.array(img.reshape((784,1)))
        self.picture = self.picture/self.picture.max(axis=0)
        self.PicFlag = 1
        #print("image: ",image)
##        plt.axis("off")
##        plt.imshow(image)
##        plt.show()
##        #print('picture: ',picture)
            
        
    
def main():
    app = QtGui.QApplication(sys.argv)
    w = MainWindow()
    w.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()        

